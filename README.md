# React-Typescript

## TLDR

React project with typescript and configured with webpack and babel. Uses prettier and
eslint for code formatting and highlighting errors.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### `npm run lint`

Fixes every error (fixable by eslint eg: unused variables, prettier format rules, etc) captured by eslint.

### `npm run format`

Formats codebase style.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

### `npm test`

N/A

### Analyzing the Bundle Size

When you you run `npm run build` a window opens up with the bundle sizes of your project.

### Advanced Configuration

To setup enviroment variables for development and production you need to add them on the `webpack define plugin function` on\
`webpack.dev.js` and `webpack.prod.js` respectively.

To change port, just edit the port field in `devServer` on the `webpack.dev.js`

### Deployment

N/A
