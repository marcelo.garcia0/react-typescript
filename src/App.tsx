import React from 'react';
import { Route, Routes } from 'react-router-dom';
import './styles.scss';

export const App = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<>home</>} />
      </Routes>
    </>
  );
};
