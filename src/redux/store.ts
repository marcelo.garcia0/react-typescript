import { configureStore } from '@reduxjs/toolkit';
import ExampleReducer from './reducers/example';

export const store = configureStore({
  reducer: { exampleState: ExampleReducer },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
